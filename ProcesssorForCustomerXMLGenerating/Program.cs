﻿using Otus.Teaching.Concurrency.Import.Core.Loaders;
using Otus.Teaching.Concurrency.Import.DataGenerator.Generators;
using ProcesssorForCustomerXMLGenerating.Infrastucture;

if (args.Length == 2)
{
    var filePath=args[0];
    int customerNumber = 1000000;
    int.TryParse(args[1],out customerNumber);
    var fc=new FileCreator(filePath,customerNumber);
}
else
{
    Console.WriteLine("Incorrect argument");
}

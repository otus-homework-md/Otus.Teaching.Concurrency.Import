﻿using System;
using System.Configuration;
using System.Diagnostics;
using System.IO;
using System.Reflection;
using Microsoft.EntityFrameworkCore;
using Otus.Teaching.Concurrency.Import.Core.Loaders;
using Otus.Teaching.Concurrency.Import.DataGenerator.Generators;
using Otus.Teaching.Concurrency.Import.Loader.Context;
using Otus.Teaching.Concurrency.Import.Loader.Loaders;

namespace Otus.Teaching.Concurrency.Import.Loader
{
    class Program
    {
        private static string _dataFilePath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "customers.xml");
        
        static void Main(string[] args)
        {
            //var optionsBuilder = new DbContextOptionsBuilder<CustomerContext>();

            //var options = optionsBuilder.UseNpgsql(@ReadSetting("DBConnectionString")).Options;

            

            if (args != null && args.Length == 1)
            {
                _dataFilePath = args[0];
            }

            Console.WriteLine($"Loader started with process Id {Process.GetCurrentProcess().Id}...");

            if (bool.Parse(ReadSetting("IsMethodGenerateData")))
                 GenerateCustomersDataFile();
            else if (bool.Parse(ReadSetting("IsProcessGenerateData")))
            {
                ProcessStartInfo procInfo = new ProcessStartInfo();
                procInfo.UseShellExecute = false;
                procInfo.FileName = @ReadSetting("ProcessGeneratorApplicationDataPath");
                procInfo.Arguments = ReadSetting("ProcessGeneratorOutputFileName") + " " + ReadSetting("ProcessGeneratorOutputRecordsCount");
                Process.Start(procInfo);
            }


            using (CustomerContext db = new CustomerContext())
            {
                //var loader = new FakeDataLoader();


                var loader = new ThreadDataLoader(db);

                loader.LoadData();

                loader.endEvent.WaitOne();
                
            }
            Console.ReadLine();
            
        }

        static void GenerateCustomersDataFile()
        {
            
            var xmlGenerator = new XmlGenerator(ReadSetting("ProcessGeneratorOutputFileName")+".xml", int.Parse(ReadSetting("ProcessGeneratorOutputRecordsCount")));
            xmlGenerator.Generate();
        }

        public static string ReadSetting(string key)
        {
            

            string result=null;
            try
            {
                var appSettings = ConfigurationManager.AppSettings;
                result = appSettings[key] ?? "Not Found";                
            }
            catch (ConfigurationErrorsException)
            {
                Console.WriteLine("Error reading app settings");
            }
           
            return result;
            
        }
    }
}
﻿using Otus.Teaching.Concurrency.Import.DataGenerator.Generators;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProcesssorForCustomerXMLGenerating.Infrastucture;

internal class FileCreator
{
    public FileCreator(string filePath, int customerNumber)
    {
        var xmlGenerator = new XmlGenerator(filePath, customerNumber);
        xmlGenerator.Generate();
    }
}

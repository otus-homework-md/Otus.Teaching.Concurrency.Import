﻿using Microsoft.EntityFrameworkCore;
using Otus.Teaching.Concurrency.Import.Core.Loaders;
using Otus.Teaching.Concurrency.Import.Loader.Context;
using Otus.Teaching.Concurrency.Import.Handler.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;
using System.IO;
using System.Runtime.Serialization;
using Otus.Teaching.Concurrency.Import.DataGenerator.Dto;
using Bogus.Bson;
using System.Threading;
using System.Diagnostics;

namespace Otus.Teaching.Concurrency.Import.Loader.Loaders
{

    public static class Extensions
    {
        public static List<List<T>> partition<T>(this List<T> values, int chunkSize)
        {
            return values.Select((x, i) => new { Index = i, Value = x })
                .GroupBy(x => x.Index / chunkSize)
                .Select(x => x.Select(v => v.Value).ToList())
                .ToList();
        }
    }


    internal class ThreadDataLoader : IDataLoader
    {

        CustomerContext db;

        CustomersList customerList =new CustomersList();

        int treadNumber;

        int chunckNumber;

        CountdownEvent cde;

        public ManualResetEvent endEvent=new ManualResetEvent(false);

        object _lock=new object();

        public ThreadDataLoader(CustomerContext db,  int tNum=2)
        {
            this.db = db;
            this.treadNumber = tNum;
            this.cde = new CountdownEvent(tNum);
            this.chunckNumber = int.Parse(Program.ReadSetting("ProcessGeneratorOutputRecordsCount"))/ tNum;
        }

        public void LoadData()
        {
            Stopwatch sw=new Stopwatch();

            XmlSerializer xmlSerializer = new XmlSerializer(typeof(CustomersList));

            // десериализуем объект
            var filename= Path.GetDirectoryName(@Program.ReadSetting("ProcessGeneratorApplicationDataPath"))+"\\"+ Program.ReadSetting("ProcessGeneratorOutputFileName")+".xml";

            using (FileStream fs = new FileStream(filename, FileMode.OpenOrCreate))
            {
                customerList = xmlSerializer.Deserialize(fs) as CustomersList;
            }

            List<List<Customer>> partitions = customerList.Customers.partition(chunckNumber);

           
            sw.Start();

            foreach(var part in partitions)
            {
                var t = new Thread(new ParameterizedThreadStart(LoadChunk));
                t.Start(part);
            }

            cde.Wait();
            sw.Stop();


            Console.WriteLine($"Saving data in {treadNumber} thread take - {sw.ElapsedMilliseconds} ms") ;

            cde.Dispose();

            TruncateTable();

            sw.Restart();   

            threadPoolLoadData(partitions);

            cde.Wait();
            sw.Stop() ;

            Console.WriteLine($"Saving data in threadpool take - {sw.ElapsedMilliseconds} ms");

            cde.Dispose();

            TruncateTable();

            endEvent.Set();


        }

        void LoadChunk(object? obj)
        {
            List<Customer> cl =  obj as List<Customer>;

                foreach (var c in cl)
                {
                   lock (_lock) { db.Customers.Add(c); }                   
                }
            lock (_lock) { db.SaveChanges(); }
                

           cde.Signal();
        }


        void TruncateTable()
        {
            foreach (var item in db.Customers)
            {
                db.Customers.Remove(item);
            }
            db.SaveChanges();

            Console.WriteLine("Database was clean");
        }


        void threadPoolLoadData(List<List<Customer>> partitions)
        {
            this.cde = new CountdownEvent(treadNumber);

            foreach (var part in partitions)
            {
                ThreadPool.QueueUserWorkItem(LoadChunk, part);
            }
        }
    }

    
}

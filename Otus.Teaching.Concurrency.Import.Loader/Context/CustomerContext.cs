﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Otus.Teaching.Concurrency.Import.Handler.Entities;
using System.Resources;
using Otus.Teaching.Concurrency.Import.Loader;


namespace Otus.Teaching.Concurrency.Import.Loader.Context
{

    public class CustomerContext : DbContext
    {
        public CustomerContext()
            : base()
        {
            //Database.EnsureDeleted();   // удаляем бд со старой схемой
            //Database.EnsureCreated();   // создаем бд с новой схемой
        }

        public DbSet<Customer> Customers { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            //var builder = WebApplication.CreateBuilder(args);

            optionsBuilder.UseNpgsql("Host=localhost;Port=5432;Database=postgres;Username=postgres;Password=postgres");
        }

        //protected override void OnModelCreating(ModelBuilder modelBuilder)
        //{
        //    modelBuilder.HasDefaultSchema("MultiProcessingThreadingHW");
        //    modelBuilder.UseSerialColumns();
        //    modelBuilder.ApplyConfiguration(new CustomerConfiguration());
        //}


    }
}
